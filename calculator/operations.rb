require 'net/http'
require_relative '../extra_operations'
require 'json'

module Calculator
    
    include ExtraOperations

    class Operations
        
        def no_integers (numeros)
            numeros = numeros.split(' ') #Transforma a string em um vetor, onde cada posição contém um valor
            resposta = []#cria um vetor que receberá S ou N em cada posição
            for numero  in numeros #para cada número no vetor:
                numero = numero.split(//)#separamos os algarismos desse número

                #As únicas possibilidades de um número ser divisível por 25, são:
                if (numero[-2] == "0" and numero[-1] == "0")#caso os últimos algarismos sejam 0 e 0,
                    resposta.push('S ')
                elsif (numero[-2] == "2" and numero[-1] == "5")#caso os últimos algarismos sejam 2 e 5,
                    resposta.push('S ')
                elsif (numero[-2] == "5" and numero[-1] == "0")#caso os últimos algarismos sejam 5 e 0,
                    resposta.push('S ')
                elsif (numero[-2] == "7" and numero[-1] == "5")# ou caso os últimos algarismos sejam 7 e 5.
                    resposta.push('S ')
                else #Caso contrário o número não será divisível por 25
                    resposta.push('N ')
                end    
            end

            return resposta #retornamos o vetor com as respostas
        end

        def baised_mean(grades, blacklist)
            
            grades = JSON.parse(grades) #transforma a string JSON em uma hash
            blacklist = blacklist.split(' ') #Transforma a string em um vetor, onde cada posição contém um nome
            
            for nome in blacklist
                grades.delete(nome) #remove da hash grades os alunos presentes na blacklist
            end
            
            soma = 0; #iniciamos o valor da soma como 0
            grades.each do |nome, nota| #para cada nome ainda presente na hash
                soma += nota #somamos o valor da nota desse nome ao valor de soma
            end
            soma = soma.to_f #convertemos a soma para float, para que, caso a média possua casas decimais, essa não sejam truncadas na divisão de inteiros
            media = soma/grades.length #a média é o valor da soma dividida pelo número de alunos restantes na hash
            return media #o valor da média é retornado   
        end

        def filter_films(genres, year)
            films = get_films #recebemos a hash com a lista de filmes
            movies = films[:movies] #alocamos o vetor de filmes na variável movies
            genres = genres.split(' ')#Transforma a string em um vetor, onde cada posição contém um gênero

            movies_list = [] #cria um vetor que retornará a lista de filmes desejada 

            movies = movies.select do |m| #para cada hash em movies:
                m[:year].to_i >= year.to_i #selecionamos os elementos(filmes) em que o ano de lançamento do filme é maior ou igual ao ano fornecido
            end

            movies.each do |m|#para cada elemento em movies:
                cont = 0 #declarmaos um contador
                for genre in genres #e verificamos, para cada gênero na string fornecida
                    if(m[:genres].include?(genre))# se este gênero está presente no elemento em questão 
                        cont += 1 #caso isso ocorra, incrementamos o contador
                    end    
                end

                if(genres.length == cont) #caso o contador seja igual ao tamanho da string de gêneros, significa que esse filme contém todos os gêneros passados
                    movies_list.push(m[:title]) #dessa forma, passamos o nome desse filme para a lista de filmes retornada
                end    
            end

            return movies_list #retornamos a lista de filmes
        end

        private def get_films
            url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json'
            uri = URI(url)
            response = Net::HTTP.get(uri)
            return JSON.parse(response, symbolize_names: true)
        end

    end

end  