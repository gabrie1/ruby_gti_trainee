require_relative 'operations'

module Calculator
    class Menu
        
        def initialize
            #definimos a interface do menu:
            puts " -------------------------"
            puts "| Bem vindo a calculadora |"
            puts " -------------------------"

            puts "\n1. Média preconceituosa"
            puts"2. Calculadoara sem números"
            puts"3. Filtro de filmes"
            puts"4. Verificador de primos"
            puts"5. Fibonacci"
            puts"6. Sair"

            puts"\nSua opção:" 
            
            @option = gets.chomp #inicializamos uma variável que receberá a opção escolhida

            operations = Operations.new #construimos um objeto operations
            extra_operations = ExtraOperations::Extra_Operations.new #construimos um objeto extra_operations

            #dependendo da opção escolhida, pedimos para o usuário digitar os parâmetros que serão utilizados,
            #passamos esses parâmetros para o método correspondete e retornamos a resposta desse método:
            if(@option == '1')
                puts "\nDigite uma String em JSON incluindo os nomes dos alunos e suas notas:"
                grades = gets.chomp
                puts "\nDigite os nomes dos alunos que serão desconsiderados, separados por espaço:"
                blacklist = gets.chomp
                result = operations.baised_mean(grades, blacklist)
                puts"\nA média preconceituosa é: #{result}"

                puts"\nPressione ENTER para continuar:"
                aux = gets.chomp

            elsif(@option == '2')
                puts"\nDigite uma sequência de números separados por espaço:"
                numeros = gets.chomp
                result = operations.no_integers(numeros)
                puts result

                puts"\nPressione ENTER para continuar:"
                aux = gets.chomp

            elsif(@option == '3')
                puts"\nDigite os gêneros do filme separados por espaço:"
                genres = gets.chomp
                puts"\nDigite a partir de que ano ocorreu o lançamento do filme:"
                year = gets.chomp
                result = operations.filter_films(genres,year)
                puts result

                puts"\nPressione ENTER para continuar:"
                aux = gets.chomp

            elsif(@option == '4')
                puts"\nDigite uma sequência de números separados por espaço:"
                numeros = gets.chomp
                result = extra_operations.primos(numeros)
                puts result

                puts"\nPressione ENTER para continuar:"
                aux = gets.chomp

            elsif(@option == '5')
                puts"\nDigite uma posição da sequência de Fibonacci"
                index = gets.chomp
                result = extra_operations.fibonacci(index)
                puts"\nO valor equivalente a essa posição é: #{result}"
                
                puts"\nPressione ENTER para continuar:"
                aux = gets.chomp
                
            elsif(@option == '6')
                puts"\nObrigado por utilizar"
                exit
            else #caso a opção digitada não seja válida, imprimimos uma mensagem de erro
                puts"\nOpção inválida!!"
                puts"\nPressione ENTER para continuar:"
                aux = gets.chomp
            end
        end
    end
end