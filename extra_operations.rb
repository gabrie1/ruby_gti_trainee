module ExtraOperations
    
    class Extra_Operations
        
        #Recebe uma string de numeros separados por espaço e retorna S, caso um
        #número seja primo, ou N, caso contrário
        def primos(numeros)
            numeros = numeros.split(' ')#Transforma a string em um vetor, onde cada posição contém um valor
            resposta = [] #cria um vetor que receberá S ou N em cada posição

            numeros.each do |numero|
                count = 0 #verificará se o número passado apresenta algum divisor

                # i é uma variável que variará de 2 até numero/2,
                #intervalo onde se encontram os possíveis divisores do número dado
                i = 2
                numero = numero.to_i #converte o numero para o tipo inteiro 

                while(i <= numero/2)
                    if(numero % i == 0)#verifica se o número é divisível por i
                        count += 1 #caso o número apresente algum divisor, o contador é incrementado
                        break #o laço é interrompido pois já sabemos que o número não é primo
                    end
                    i += 1
                end
                
                if(count != 0) #se o contador não for zero, significa que o contador foi incrementado
                    resposta.push('N') # desse modo o número não é primo
                elsif(numero == 1 or numero == 0) #se o número for 1 ou 0 ele não será primo,
                    resposta.push('N') # no entanto, o contador não será incrementado, criamos então um caso especial    
                else #caso o contador não tenha sido incrementado e o número seja diferente de 1 e 0
                    resposta.push('S') # o número será primo 
                end
            end
            return resposta #retornamos o vetor com as respostas
        end

        #Recebe um índice e retorna o elemento da sequência de fibonacci equivalente a esse índice
        #OBS.: O primeiro elemento da sequência equivale ao índice da posição 1
        def fibonacci(index)

            #lembre-se que, em fibonacci, um termo da sequência equivale à soma dos dois termos anteriores
            #Os dois primeiros termos da sequência valem 1

            i = 2 #um contador
            var1 = 1 #variável auxiliar 1 (representa o ante-penúltimo elemento)
            var2 = 1 #variável auxiliar 2 (representa o penúltimo elemento)
            resposta = 1 #valor que será retornado

            while(i < index.to_i)#Esse loop se repete até que atinjamos o índice desejado:
                resposta = var1 + var2 #o valor da resposta equivale à soma dos dois valores anteriores
                var1 = var2 #o valor do ante-penúltimo elemento é atualizado
                var2 = resposta #o valor do penúltimo elemento é atualizado
                i += 1 # o i é icrementado
            end 

            if(index.to_i < 1) #Caso o valor passado seja inválido
                return "inválido" #retornaremos uma resposta de erro
            end

            return resposta #A resposta é retornada
            #Note que,caso o índice passado seja 1 ou 2, não haverá entrada no loop while e,
            #portanto, o retorno será 1, já que inicializamos "resposta" valendo 1
        end    
    end                    

end
